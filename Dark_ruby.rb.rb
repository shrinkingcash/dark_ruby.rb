# clear_screens the screen fast and easy.
# 
# Use: clear_screen_screen()
# => clear_screens the terminal.

def clear_screen
    #system("clear")
    system("cls")
end

# If the game throws an error and you catch it you may use this
# to alert the player that there may be a problem.
# Use: error_message()
# => Displays an error message.

def error_message
	clear_screen()
	puts "An error has occurred. Please submit a bug report."
	gets
end

#this is the enemy and is not yet done
#this is the enemey life and damage to the player
def enemy_attackscreen
	attack = $attack 
	if attack <= 1
		life_enemy = $enemy_life
		life_player = $health_player
	else
		life_enemy = $new_life_enemy
		life_player = $new_life_player
	end

	total_life_enemy = $enemy_life
	attack_enemy = $enemy_attack
	magic_enemy = $magic_enemy

	total_life_player = $health_player
	attack_player = $attack_player
	magic_player = $magic_player

	puts "##########Enemy##########"
	puts "Name:  #{$enemy_name}"
	puts "Life:  #{life_enemy}/#{total_life_enemy}"
	puts "Attack: #{$enemy_attack}"
	puts "Magic: #{$magic_enemy}"

	puts ""

        puts "##########You##########"
	puts "Name:   #{$name_player}"
	puts "Life:   #{life_player}/#{total_life_player}"
	puts "Attack: #{$attack_player}"
	puts "Magic:  #{$magic_player}"
	gets
	if life_player <= 0
	    dead_player
	end
	if life_enemy <= 0
	    dead_enemy
	end
	answer = 1
	if answer == 1
		clear_screen
		attack_bonus = attack_enemy + 2
		damage = 0 + rand(attack_bonus)
		damage.to_i
		life_player = life_player - damage
		$new_life_player = life_player
		if damage == 0
			$attack = attack + 1
			puts "He miss"
			gets
			attack_screen
		elsif damage > attack_player
			$attack = attack + 1
			bonus_damage = damage - attack_player
			damage = damage - bonus_damage
			puts "He did extra damage #{damage} + bonus damage: #{bonus_damage}"
			puts "Your health is now  #{life_player} life"
			gets
		    attack_screen
		else
			$attack = attack + 1
			puts "He did              #{damage} damage"
			puts "Your health is now: #{life_player} life" 
			gets
			attack_screen
		end
	end
end
def attack_screen
	attack = $attack
	clear_screen

	if attack < 1
		life_enemy = $enemy_life
		life_player = $health_player
	else
		life_enemy = $new_life_enemy
		life_player = $new_life_player
	end


	total_life_enemy = $enemy_life
	attack_enemy = $enemy_attack
	magic_enemy = $magic_enemy
	puts "##########Enemy##########"
	puts "Name:  #{$enemy_name}"
	puts "Life:  #{life_enemy}/#{total_life_enemy}"
	puts "Attack: #{$enemy_attack}"
	puts "Magic: #{$magic_enemy}"

	puts ""
   
        total_life_player = $health_player
	attack_player = $attack_player
	magic_player = $magic_player
	puts "##########You##########"
	puts "Name:   #{$name_player}"
	puts "Life:   #{life_player}/#{total_life_player}"
	puts "Attack: #{$attack_player}"
	puts "Magic:  #{$magic_player}"
	
	puts ""
	
	puts "##########Option##########"
	puts "1: Attack"
	puts "2: Magic"
	puts "3: Block"
	puts "4: Potion"
	puts "5: Try to run"
	puts "6: Info about character"
	answer = gets.to_i
	if life_player <= 0
	    dead_player
	end
	if life_enemy <= 0
	    dead_enemy
	end
	if answer == 1
		clear_screen
		attack_bonus = attack_player + 2
		damage = 0 + rand(attack_bonus)
		damage.to_i
		life_enemy = life_enemy - damage
		$new_life_enemy = life_enemy
		if damage == 0
			$attack = attack + 1
			puts "You miss"
			gets
			enemy_attackscreen
		elsif damage > attack_player
			$attack = attack + 1
			bonus_damage = damage - attack_player
			damage = damage - bonus_damage
			puts "You did extra damage #{damage} + bonus damage: #{bonus_damage}"
			puts "Enemy health is now  #{life_enemy} life"
			gets
		    enemy_attackscreen
		else
			$attack = attack + 1
			puts "You did              #{damage} damage"
			puts "Enemy health is now: #{life_enemy} life" 
			gets
			enemy_attackscreen
		end
	elsif answer == 6
	    stats
	    gets
	    attack_screen
	end
end
def enemy(enemy)
    if enemy == 1
     	$enemy_life = 10
     	$enemy_attack = 2
     	$enemy_name = "bandits"
     	$magic_enemy = 0
    end 
end

#this showes the stats
def stats
	clear_screen
	puts "Stats"
	puts "Name:   #{$name_player}"
	puts "Gender: #{$gender}"
	puts "Race:   #{$race_player}"
	puts "Level:  #{$level_player}"
	puts "Exp:    #{$exp_player}/#{$exptolevelup_player}"
	puts "Zone:    #{$map}"
	puts "Attack: #{$attack_player}"
	puts "Health: #{$health_player}"
	puts "stalth: #{$stalth_player}"
	puts "Magic:  #{$magic_player}"	
	puts "Range:  #{$range_player}"
	puts "Speed:  #{$speed_player}"
	gets
end

def map2
    $map_part = 2
    puts "In a castle up in the sky you awakes"
    gets
    dead_player
end
#this is the first map
#name = The dark woods
def map1
        $map_part = 1
	clear_screen
	puts "In the big dark woods you awakes"
	puts "you looking around and see nothing then trees and bushes"
	puts "a man apears from nothing and asks you"
	puts "Strange man: what are you doing in the dark woods?"
	puts "you answer:"
	puts "1: I don`t know"
	puts "2: I am on a mission"
	puts "3: I gona kill you"
	answer = gets.to_i
	if answer == 1
		clear_screen
		puts "Strange man: I can help you out of the woods"
		puts "1: Follow"
		puts "2: Say no i find my self out of the woods"
		answer = gets.to_i
		if answer == 1
			clear_screen
			puts "You and the Strange man walks for some time and som bandits showes up:"
			puts "Bandits: give me all the money"
			puts "What do you do?"
			puts "1: Attack "
			puts "2: Giveing all the money"
			answer = gets.to_i
			if answer == 1
				clear_screen
				enemy(1)
				$attack = 0
				attack_screen
			end
		else 
			error
		end
	elsif answer == 2
		clear_screen
		puts "Strange man: On what mission?"
		puts "you answer"
		puts "1: A secret mission"
		puts "2: A mission to find the prinsess"
		puts "3: A bloody massakary mission"
		answer = gets.to_i
	elsif answer == 3
	    clear_screen 
		puts "Strange man: why?"
		puts "you answer:"
		puts "1: I want to"
		puts "2: That is my mission"
		answer = gets.to_i
	else
		error
	end

	gets
	zone(2)
end

#this chose the map the player is on
def zone(map)
	if map == 1
		$map = "The dark woods"
		map1
	elsif map == 2
		$map = "The castle"
		map2
	else
		error
	end
end

#this makes the total exp that needed to level up
def exp
	exp = $exp_player
	exptolevelup = exp * 10
	$exptolevelup_player = exptolevelup  
end

def gender 
gender = $gender
	if gender == 1
		$gender = "Girl"
	elsif gender == 2
		$gender = "Boy"
	else
		error
	end
end

#this saves the class the player have choisen
def character(race, health, range, attack, speed, hidden, magic)
	if race == 1
		race = "Elf"
	elsif race == 2
		race = "Orc"
	elsif race == 3
		race = "Mage"
	end

	clear_screen
	puts "Gender of the character:"
	puts "1: Girl"
	puts "2: Boy"
	$gender = gets.to_i
	clear_screen
	gender

	puts "Name on character:"
	character_name = gets.to_s
	level = 1
	clear_screen
	puts "info"
	puts "Name:   #{character_name}"
	puts "Level:  #{level}"
	puts "Race:   #{race}"
	puts "health: #{health}"
	puts "range:  #{range}"
	puts "attack: #{attack}"
	puts "speed:  #{speed}"
	puts "stalth: #{hidden}"
	puts "magic:  #{magic}"
	gets
        $stalth_player = hidden
	$magic_player = magic
	$range_player = range
	$speed_player = speed
	$name_player = character_name
	$race_player = race
        $health_player = health
        $attack_player = attack
        $level_player = level
        $exp_player = 1
        exp
        zone(1)
end

#a class
def race_elf
	character(1, 10, 5, 5, 8, 5, 3)
end

#a other class
def race_orc
	character(2, 20, 1, 7, 2, 1, 1)
end

#and the last class
def race_mage
	character(3, 7, 4, 5, 6, 3, 8)
end

# This part will show the player a character selection screen
# where he will be able to chose a character and name it.
# Use: character_screen()
# => Shows a character selection screen.

def character_screen
    clear_screen
    puts "Choose a race:"
    puts "1: Elf"
    puts "2: Orc"
    puts "3: Mage"
    puts "4: Info"
    puts "5: Exit"
    option = gets.to_i
    if option == 1
        clear_screen()
        race_elf
    elsif option == 2
        clear_screen()
        race_orc
    elsif option == 3 
        clear_screen()
        race_mage
    elsif option == 4 
        clear_screen()
        puts "Elfs are good at range and agility, they are really fast."
        puts "Orcs are great in near combat, they also have a lot of life."
        puts "Mages are ranged and have magical powers, however they don't have much health."
        character_screen()
    elsif option == 5
        abort()
    else
        puts "You need to choose a class in order to proceed."
    end
end

# This method starts the game and prompts the user
# with various selection screens.
# Use: start_game()
# => Starts a new game.

def start_game
    clear_screen()
    puts "What is your name?"
    user = gets
    clear_screen()
    puts "Hello #{user}."
    clear_screen()
    character_screen
end

def dead_enemy
    clear_screen()
    puts "The enemy is dead"
    #puts "You got #{exp_gained}"
    gets
    map = $map_part
    map.to_i
    map = map + 1
    zone(map)
end
def dead_player
    clear_screen()
    puts "You are dead!"
    puts "restart (press enter)"
    gets
    map = $map_part
    map.to_i
    map = map + 1
    zone(map)
end

start_game()